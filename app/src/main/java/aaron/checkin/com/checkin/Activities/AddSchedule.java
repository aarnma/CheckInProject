package aaron.checkin.com.checkin.Activities;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.isapanah.awesomespinner.AwesomeSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import aaron.checkin.com.checkin.Helper.ActionCallback;
import aaron.checkin.com.checkin.Helper.CheckHttp;
import aaron.checkin.com.checkin.Helper.TimePickerFragment;
import aaron.checkin.com.checkin.Helper.URL;
import aaron.checkin.com.checkin.R;

import static aaron.checkin.com.checkin.Helper.URL.timeFormate;

/**
 * Created by aaron on 6/15/2018.
 */

public class AddSchedule extends AppCompatActivity implements View.OnClickListener {


    private AwesomeSpinner staff;
    private EditText start_date;
    private EditText end_date;
    private EditText start_time;
    private EditText end_time;
    private Button add;
    private Calendar myCalendar;
    private Date start , end, s_time, e_time;
    public final String inputFormat = "HH:mm";
    SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.CANADA);

    String myFormat = "yyyy-MM-dd";
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.CANADA);

    private String start_time_txt="";
    private String end_time_txt="";
    private String start_date_str="";
    private String end_date_str="";

    private boolean modfiy = false;
    private boolean request = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_schedule);

        Toolbar toolbar = findViewById(R.id.add_schedule_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        myCalendar = Calendar.getInstance();
        staff = findViewById(R.id.staff_list);
        start_date = findViewById(R.id.start_date);
        end_date = findViewById(R.id.end_date);
        start_time = findViewById(R.id.start_time);
        end_time = findViewById(R.id.end_time);
        add = findViewById(R.id.btnAddSchedule);
        TextView title = findViewById(R.id.add_title);

        ArrayAdapter<CharSequence> staffAdapter = ArrayAdapter.createFromResource(AddSchedule.this, R.array.staff_name, android.R.layout.simple_spinner_item);
        staff.setAdapter(staffAdapter, 0);
        staff.setOnSpinnerItemClickListener(new AwesomeSpinner.onSpinnerItemClickListener<String>() {
            @Override
            public void onItemSelected(int position, String itemAtPosition) {

            }
        });


        Intent intent = getIntent();
        if(intent.hasExtra("start_time")){
            start_time_txt = getIntent().getStringExtra("start_time");
            end_time_txt = getIntent().getStringExtra("end_time");
            start_date_str = getIntent().getStringExtra("date");
            end_date_str = getIntent().getStringExtra("date");
            int index = Arrays.asList(getResources().getStringArray(R.array.staff_name)).indexOf(getIntent().getStringExtra("name"));
            staff.setSelection(index);

            title.setText("Modify Schedule");
            add.setText("Save");

            start_date.setText(start_date_str);
            end_date.setText(end_date_str);

            String[] startTime = start_time_txt.split(":");
            start_time.setText(timeFormate(Integer.valueOf(startTime[0]), Integer.valueOf(startTime[1])));
            String[] endTime = end_time_txt.split(":");
            end_time.setText(timeFormate(Integer.valueOf(endTime[0]), Integer.valueOf(endTime[1])));

            start = parseDateTime(start_date_str);
            end = parseDateTime(end_date_str);
            s_time = parseDate(start_time_txt);
            e_time = parseDate(end_time_txt);

            modfiy = true;
        }

        start_date.setOnClickListener(this);
        end_date.setOnClickListener(this);
        start_time.setOnClickListener(this);
        end_time.setOnClickListener(this);
        add.setOnClickListener(this);

    }

    @Override
    public boolean onSupportNavigateUp() {
        if(request){
            Intent intent = new Intent();
            intent.putExtra("name", staff.getSelectedItem());
            setResult(RESULT_OK, intent);
        }
        onBackPressed();
        return true;
    }

    @Override
    public void onClick(final View view) {
        switch (view.getId()) {

            case R.id.start_date:

                DatePickerDialog.OnDateSetListener from_date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view1, int year, int monthOfYear,
                                          int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        start_date_str = sdf.format(myCalendar.getTime());

                        if(end==null){
                            start = parseDateTime(start_date_str);
                            start_date.setText(start_date_str);}
                        else{
                            if(!parseDateTime(start_date_str).after(end)){
                                start = parseDateTime(start_date_str);
                                start_date.setText(start_date_str);
                            }
                            else{
                                Snackbar.make(view, R.string.date_warning, Snackbar.LENGTH_LONG).show();
                            }
                        }
                    }

                };

                new DatePickerDialog(AddSchedule.this, from_date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.end_date:

                DatePickerDialog.OnDateSetListener to_date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view1, int year, int monthOfYear,
                                          int dayOfMonth) {

                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                       end_date_str = sdf.format(myCalendar.getTime());

                        if(start==null){
                            end = parseDateTime(end_date_str);
                            end_date.setText(end_date_str);}
                        else{
                            if(!parseDateTime(end_date_str).before(start)){
                                end = parseDateTime(end_date_str);
                                end_date.setText(end_date_str);
                            }
                            else{
                                Snackbar.make(view, R.string.date_warning, Snackbar.LENGTH_LONG).show();
                            }
                        }
                    }

                };
                new DatePickerDialog(AddSchedule.this, to_date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

                break;

            case R.id.start_time:

                DialogFragment dFragment = new TimePickerFragment(new TimePickerFragment.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view1, int hourOfDay, int minute) {
                        start_time_txt = hourOfDay + ":" + minute;
                        if(e_time==null){
                            s_time = parseDate(start_time_txt);
                            setTime(hourOfDay, minute, start_time);}
                        else{
                            if(parseDate(start_time_txt).before(e_time)){
                                s_time = parseDate(start_time_txt);
                                setTime(hourOfDay, minute, start_time);
                            }
                            else{
                                Snackbar.make(view, R.string.time_warning, Snackbar.LENGTH_LONG).show();
                            }
                        }
                    }
                });
                dFragment.show(getFragmentManager(),"Time Picker");

                break;

            case R.id.end_time:

                DialogFragment Fragment = new TimePickerFragment(new TimePickerFragment.OnTimeSetListener(){
                    @Override
                    public void onTimeSet(TimePicker view1, int hourOfDay, int minute) {
                        end_time_txt = hourOfDay + ":" + minute;
                        if(s_time==null){
                            e_time = parseDate(end_time_txt);
                            setTime(hourOfDay, minute, end_time);}
                        else{
                            if(parseDate(end_time_txt).after(s_time)){
                                e_time = parseDate(end_time_txt);
                                setTime(hourOfDay, minute, end_time);
                            }
                            else{
                                Snackbar.make(view,  R.string.time_warning, Snackbar.LENGTH_LONG).show();
                            }
                        }
                    }
                });
                Fragment.show(getFragmentManager(),"Time Picker");


                break;

            case R.id.btnAddSchedule:
                String name = staff.getSelectedItem();

                if(staff.isSelected()&&!start_date_str.isEmpty()&&!end_date_str.isEmpty()&&!start_time_txt.isEmpty()&&!end_time_txt.isEmpty()){
                    final JSONObject checkoutObject = new JSONObject();
                    try {
                        checkoutObject.put("name", name);
                        checkoutObject.put("from_date", start_date_str);
                        checkoutObject.put("to_date", end_date_str);
                        checkoutObject.put("start_time", start_time_txt);
                        checkoutObject.put("end_time", end_time_txt);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String url;
                    if(modfiy){
                        url = URL.URL_MODIFY_SCHEDULE;
                    }
                    else{
                        url = URL.URL_ADD_SCHEDULE;
                    }

                    new CheckHttp(AddSchedule.this, view).HttpJsonRequest(url, checkoutObject, new ActionCallback<JSONObject>() {
                        @Override
                        public void ok(JSONObject jsonObject) {

                            try {
                                if(jsonObject.getString("status").equals("200")){
                                    request = true;
                                }
                                Snackbar.make(view, jsonObject.getString("msg"), Snackbar.LENGTH_LONG).show();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                }else{
                    Snackbar.make(view, "Please fill up all fields!", Snackbar.LENGTH_LONG).show();
                }

                break;

        }
    }

    private Date parseDate(String date) {

        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }

    private Date parseDateTime(String date) {

        try {
            return sdf.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }

    @SuppressLint("DefaultLocale")
    private void setTime(int hourOfDay, int minute, EditText view){

        int hour = hourOfDay % 12;
        view.setText(String.format("%02d:%02d %s", hour == 0 ? 12 : hour,
                minute, hourOfDay < 12 ? "am" : "pm"));

    }


}