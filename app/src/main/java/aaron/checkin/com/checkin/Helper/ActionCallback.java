package aaron.checkin.com.checkin.Helper;

/**
 * Created by zhew on 2/8/18.
 **/
public interface ActionCallback<JSONObject> {
    void ok(JSONObject jsonObject);
}