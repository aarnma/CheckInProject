package aaron.checkin.com.checkin;

/**
 * Created by Programmer 1 on 2018-01-17.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import aaron.checkin.com.checkin.Helper.ActionCallback;
import aaron.checkin.com.checkin.Helper.CheckHttp;
import aaron.checkin.com.checkin.Helper.SQLiteHandler;
import aaron.checkin.com.checkin.Helper.SessionManager;
import aaron.checkin.com.checkin.Helper.URL;


public class LoginActivity extends AppCompatActivity {

    private Button btnLogin;
    private EditText inputEmail;
    private EditText inputPassword;
    private ProgressDialog pDialog;
    private SessionManager session;
    private SQLiteHandler db;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        inputEmail = findViewById(R.id.email);
        inputPassword = findViewById(R.id.password);
        btnLogin = findViewById(R.id.btnLogin);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(true);

        // SQLite database handler
        db = new SQLiteHandler(getApplicationContext());

        // Session manager
        session = new SessionManager(getApplicationContext());

        // Check if user is already logged in or not
        if (session.isLoggedIn()) {
            // User is already logged in. Take him to main activity
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }


        Toolbar toolbar = findViewById(R.id.login_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        // Login button Click Event
        btnLogin.setOnClickListener(new View.OnClickListener() {

            public void onClick(View view) {
                String email = inputEmail.getText().toString().trim();
                String password = inputPassword.getText().toString().trim();

                // Check for empty data in the form
                if (!email.isEmpty() && !password.isEmpty()) {

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("name", email);
                        jsonObject.put("password", password);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    pDialog.setMessage("Logging in");
                    showDialog();

                    new CheckHttp(LoginActivity.this, getCurrentFocus()).HttpJsonRequest(URL.URL_LOGIN, jsonObject, new ActionCallback<JSONObject>() {
                        @Override
                        public void ok(JSONObject jsonObject) {

                            try {
                                if(jsonObject.getString("status").equals("200")){
                                    db.addUser( jsonObject.getString("name"),
                                            jsonObject.getString("right"));
                                    session.setLogin(true);
                                    Intent intent = new Intent(LoginActivity.this,
                                            MainActivity.class);
                                    startActivity(intent);
                                    finish();
                                }else{
                                    Snackbar.make(getCurrentFocus(), jsonObject.getString("msg"), Snackbar.LENGTH_LONG).show();
                                }
                                hideDialog();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    });

                } else {
                    // Prompt user to enter credentials
                    Snackbar.make(getCurrentFocus(), "Please enter the credentials!", Snackbar.LENGTH_LONG).show();

                }
            }

        });


    }



    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}