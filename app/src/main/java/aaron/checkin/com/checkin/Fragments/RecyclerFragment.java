package aaron.checkin.com.checkin.Fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.dinuscxj.refresh.RecyclerRefreshLayout;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import aaron.checkin.com.checkin.Helper.SQLiteHandler;
import aaron.checkin.com.checkin.Helper.SessionManager;
import aaron.checkin.com.checkin.R;
import aaron.checkin.com.checkin.adapter.HeaderViewRecyclerAdapter;
import aaron.checkin.com.checkin.adapter.RecyclerListAdapter;
import aaron.checkin.com.checkin.model.CursorModel;
import aaron.checkin.com.checkin.tips.DefaultTipsHelper;
import aaron.checkin.com.checkin.tips.TipsHelper;


public abstract class RecyclerFragment<MODEL extends CursorModel> extends Fragment {
    private boolean mIsLoading;

    private RecyclerView mRecyclerView;
    private RecyclerRefreshLayout mRecyclerRefreshLayout;

    private TipsHelper mTipsHelper;
    private HeaderViewRecyclerAdapter mHeaderAdapter;
    private RecyclerListAdapter<MODEL, ?> mOriginAdapter;

    private InteractionListener mInteractionListener;

    private final RefreshEventDetector mRefreshEventDetector = new RefreshEventDetector();
    private final AutoLoadEventDetector mAutoLoadEventDetector = new AutoLoadEventDetector();

    private Spinner staff;
    private SQLiteHandler db;
    private SessionManager session;
    private List<String> staffList = new ArrayList<>();

    protected String name;
    protected String right;

    protected int page = 2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.base_refresh_recycler_list_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        staff = getActivity().findViewById(R.id.staff_list);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.staff_name, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_draw);
        staff.setAdapter(adapter);

        db = new SQLiteHandler(getActivity());
        session = new SessionManager(getActivity());

        HashMap<String, String> user = db.getUserDetails();
        name = user.get("name");
        right = user.get("right");

        staffList=  Arrays.asList(getResources().getStringArray(R.array.staff_name));
        staff.setSelection(staffList.indexOf(name));
        staff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                name = adapterView.getItemAtPosition(i).toString();
                page = 2;
                refresh();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (Integer.valueOf(right) < 7) {
            staff.setEnabled(false);
        }

        initRecyclerView(view);
        initRecyclerRefreshLayout(view);

        mInteractionListener = createInteraction();
        mTipsHelper = createTipsHelper();

        refresh();
    }



    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {

            staff = getActivity().findViewById(R.id.staff_list);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                    R.array.staff_name, R.layout.spinner_item);
            adapter.setDropDownViewResource(R.layout.spinner_draw);
            staff.setAdapter(adapter);

            staffList=  Arrays.asList(getResources().getStringArray(R.array.staff_name));
            staff.setSelection(staffList.indexOf(name));
            staff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    name = adapterView.getItemAtPosition(i).toString();
                    page = 2;
                    refresh();
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            if (Integer.valueOf(right) < 7) {
                staff.setEnabled(false);
            }
        }
    }


    private void initRecyclerView(View view) {
        mRecyclerView = view.findViewById(R.id.recycler_view);

        mRecyclerView.addOnScrollListener(mAutoLoadEventDetector);

        RecyclerView.LayoutManager layoutManager = onCreateLayoutManager();
        if (layoutManager != null) {
            mRecyclerView.setLayoutManager(layoutManager);
        }

        mOriginAdapter = createAdapter();
        mHeaderAdapter = new HeaderViewRecyclerAdapter(mOriginAdapter);
        mRecyclerView.setAdapter(mHeaderAdapter);
        mHeaderAdapter.adjustSpanSize(mRecyclerView);
    }

    private void initRecyclerRefreshLayout(View view) {
        mRecyclerRefreshLayout = (RecyclerRefreshLayout) view.findViewById(R.id.refresh_layout);

        if (mRecyclerRefreshLayout == null) {
            return;
        }

//        mRecyclerRefreshLayout.setNestedScrollingEnabled(true);
        mRecyclerRefreshLayout.setOnRefreshListener(mRefreshEventDetector);
    }

    @NonNull
    public abstract RecyclerListAdapter createAdapter();

    protected RecyclerView.LayoutManager onCreateLayoutManager() {
        return new LinearLayoutManager(getActivity());
    }

    protected TipsHelper createTipsHelper() {
        return new DefaultTipsHelper(this);
    }

    protected InteractionListener createInteraction() {
        return null;
    }

    @Override
    public void onDestroyView() {
        mRecyclerView.removeOnScrollListener(mAutoLoadEventDetector);
        super.onDestroyView();
    }

    public HeaderViewRecyclerAdapter getHeaderAdapter() {
        return mHeaderAdapter;
    }

    public RecyclerListAdapter<MODEL, ?> getOriginAdapter() {
        return mOriginAdapter;
    }

    public RecyclerRefreshLayout getRecyclerRefreshLayout() {
        return mRecyclerRefreshLayout;
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }


    public void refresh() {
        if (isFirstPage()) {
            mTipsHelper.showLoading(true);
        } else {
            mRecyclerRefreshLayout.setRefreshing(true);
        }

        requestRefresh();
    }

    public boolean isFirstPage() {
        return mOriginAdapter.getItemCount() < 5;
    }

    public class RefreshEventDetector implements RecyclerRefreshLayout.OnRefreshListener {

        @Override
        public void onRefresh() {
            requestRefresh();
        }
    }

    public class AutoLoadEventDetector extends RecyclerView.OnScrollListener {

        @Override
        public void onScrolled(RecyclerView view, int dx, int dy) {
            RecyclerView.LayoutManager manager = view.getLayoutManager();
            if (manager.getChildCount() > 0) {
                int count = manager.getItemCount();
                int last = ((RecyclerView.LayoutParams) manager
                        .getChildAt(manager.getChildCount() - 1).getLayoutParams()).getViewAdapterPosition();

                if (last == count - 1 && !mIsLoading && mInteractionListener != null) {
                    requestMore();
                }
            }
        }
    }

    private void requestRefresh() {
        if (mInteractionListener != null && !mIsLoading) {
            mIsLoading = true;
            mInteractionListener.requestRefresh();
        }
    }

    private void requestMore() {
        if (mInteractionListener != null && !isFirstPage() && !mIsLoading) {
            mIsLoading = true;
            mInteractionListener.requestMore();
        }
    }

    public abstract class InteractionListener {
        public void requestRefresh() {
            requestComplete();

            if (mOriginAdapter.isEmpty()) {
                mTipsHelper.showEmpty();
            }else if(hasMore()){
                mTipsHelper.showHasMore();
                if(isFirstPage()){
                    mTipsHelper.hideHasMore();
                }
            }
            else {
                mTipsHelper.hideHasMore();
            }
        }

        public void requestMore() {
            requestComplete();
        }

        public void requestFailure() {
            requestComplete();
            mTipsHelper.showError(isFirstPage(), new Exception("net error"));
        }

        protected void requestComplete() {
            mIsLoading = false;

            if (mRecyclerRefreshLayout != null) {
                mRecyclerRefreshLayout.setRefreshing(false);
            }
            mRecyclerRefreshLayout.setRefreshing(false);
            mTipsHelper.hideError();
            mTipsHelper.hideEmpty();
            mTipsHelper.hideLoading();
            mTipsHelper.hideHasMore();
        }

        protected boolean hasMore() {
            return mOriginAdapter.getItem(mOriginAdapter.getItemCount() - 1).hasMore();
        }
    }
}
