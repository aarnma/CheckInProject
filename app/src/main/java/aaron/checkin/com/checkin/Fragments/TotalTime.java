package aaron.checkin.com.checkin.Fragments;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import aaron.checkin.com.checkin.Activities.AddSchedule;
import aaron.checkin.com.checkin.Helper.SQLiteHandler;
import aaron.checkin.com.checkin.R;

/**
 * Created by aaron on 6/11/2018.
 */

public class TotalTime extends Fragment implements View.OnClickListener {

    private SQLiteHandler db;
    private String name;
    private String right;
    private List<String> staffList = new ArrayList<>();

    private TextView start_date;
    private TextView end_date;

    private Calendar myCalendar;
    private Date start , end;

    private String start_date_str="";
    private String end_date_str="";

    String myFormat = "yyyy-MM-dd";
    SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.CANADA);
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_total_time, container, false);
        Spinner staff = view.findViewById(R.id.staff_list);

        start_date = view.findViewById(R.id.from);
        end_date = view.findViewById(R.id.to);
        myCalendar = Calendar.getInstance();

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.staff_name, R.layout.black_spinner);
        adapter.setDropDownViewResource(R.layout.black_draw);
        staff.setAdapter(adapter);

        db = new SQLiteHandler(getActivity());
        HashMap<String, String> user = db.getUserDetails();
        name = user.get("name");
        right = user.get("right");

        staffList=  Arrays.asList(getResources().getStringArray(R.array.staff_name));
        staff.setSelection(staffList.indexOf(name));
        staff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                name = adapterView.getItemAtPosition(i).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        if (Integer.valueOf(right) < 7) {
            staff.setEnabled(false);
        }

        start_date.setOnClickListener(this);
        end_date.setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.from:

                DatePickerDialog.OnDateSetListener from_date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view1, int year, int monthOfYear,
                                          int dayOfMonth) {
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        start_date_str = sdf.format(myCalendar.getTime());

                        if(end==null){
                            start = parseDateTime(start_date_str);
                            start_date.setText(start_date_str);}
                        else{
                            if(!parseDateTime(start_date_str).after(end)){
                                start = parseDateTime(start_date_str);
                                start_date.setText(start_date_str);
                            }
                            else{
                                Snackbar.make(getView(), R.string.date_warning, Snackbar.LENGTH_LONG).show();
                            }
                        }
                    }

                };

                new DatePickerDialog(getActivity(), from_date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;

            case R.id.to:

                DatePickerDialog.OnDateSetListener to_date = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view1, int year, int monthOfYear,
                                          int dayOfMonth) {

                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        end_date_str = sdf.format(myCalendar.getTime());

                        if(start==null){
                            end = parseDateTime(end_date_str);
                            end_date.setText(end_date_str);}
                        else{
                            if(!parseDateTime(end_date_str).before(start)){
                                end = parseDateTime(end_date_str);
                                end_date.setText(end_date_str);
                            }
                            else{
                                Snackbar.make(getView(), R.string.date_warning, Snackbar.LENGTH_LONG).show();
                            }
                        }
                    }

                };
                new DatePickerDialog(getActivity(), to_date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
                break;
            case R.id.btnFindTotalTime:
                break;


        }
    }

    private Date parseDateTime(String date) {

        try {
            return sdf.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }
}