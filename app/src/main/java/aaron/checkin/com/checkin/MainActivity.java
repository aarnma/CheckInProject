package aaron.checkin.com.checkin;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.HashMap;

import aaron.checkin.com.checkin.Fragments.CheckInOut;
import aaron.checkin.com.checkin.Fragments.PendOT;
import aaron.checkin.com.checkin.Fragments.TotalTime;
import aaron.checkin.com.checkin.Fragments.WorkSchedule;
import aaron.checkin.com.checkin.Helper.SQLiteHandler;
import aaron.checkin.com.checkin.Helper.SessionManager;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static aaron.checkin.com.checkin.Helper.URL.logoutUser;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private TextView title;

    private Fragment[] fragments;
    private int index;
    private int currentTabIndex;

    private CheckInOut checkInOut;
    private WorkSchedule workSchedule;
    private TotalTime totalTime;
    private PendOT pendOT;

    private SQLiteHandler db;
    private SessionManager session;

    private TextView username;
    private Toolbar toolbar;

    private Spinner staff;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setTitle("");
        toolbar.setSubtitle("");

        title = findViewById(R.id.check_title);
        title.setText(R.string.check_in_out);

        staff = findViewById(R.id.staff_list);

        db = new SQLiteHandler(getApplicationContext());
        session = new SessionManager(getApplicationContext());

        HashMap<String, String> user = db.getUserDetails();
        String name = user.get("name");
        String right = user.get("right");

        if (!session.isLoggedIn()) {
            logoutUser(db, session, MainActivity.this);
        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        View v = navigationView.getHeaderView(0);
        username = v.findViewById(R.id.username);
        username.setText(name);

        checkInOut = new CheckInOut();
        workSchedule = new WorkSchedule();

        totalTime = new TotalTime();
        pendOT = new PendOT();


        fragments = new Fragment[]{checkInOut, workSchedule, totalTime, pendOT};

        currentTabIndex = 0;
        getSupportFragmentManager().beginTransaction().add(R.id.main_container, checkInOut)
                .show(checkInOut)
                .commit();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.check_in_out) {
            staff.setVisibility(View.GONE);
            title.setVisibility(View.VISIBLE);
            title.setText(R.string.check_in_out);
            index = 0;
            changeFragment();
        } else if (id == R.id.work_schedule) {
            title.setText(R.string.work_schedule);
            title.setVisibility(View.GONE);
            staff.setVisibility(View.VISIBLE);
            index = 1;
            changeFragment();

        } else if (id == R.id.time) {
            staff.setVisibility(View.GONE);
            title.setVisibility(View.VISIBLE);
            title.setText(R.string.total_time);
            index = 2;
            changeFragment();
        } else if (id == R.id.pend_ot) {
            title.setVisibility(View.GONE);
            staff.setVisibility(View.VISIBLE);
            title.setText(R.string.pended_ot);
            index = 3;
            changeFragment();
        }
        else if (id == R.id.log_out) {
            new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("Are you sure to logout?")
                    .setConfirmText("Yes")
                    .setCancelText("No")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            logoutUser(db, session, MainActivity.this);
                            sDialog.cancel();
                        }
                    })
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.cancel();
                        }
                    })
                    .show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void changeFragment(){

        if (currentTabIndex != index) {
            FragmentTransaction trx = getSupportFragmentManager().beginTransaction();
            trx.hide(fragments[currentTabIndex]);
            if (!fragments[index].isAdded()) {
                trx.add(R.id.main_container, fragments[index]);
            }
            trx.show(fragments[index]).commit();
        }

        currentTabIndex = index;
    }
}
