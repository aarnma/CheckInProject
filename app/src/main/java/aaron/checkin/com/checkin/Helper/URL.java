package aaron.checkin.com.checkin.Helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;

import aaron.checkin.com.checkin.LoginActivity;

/**
 * Created by aaron on 6/15/2018.
 */

public class URL {

    private static final String root ="http://35.233.163.57:5989/api/";
    public static String URL_LOGIN = root + "log";
    public static String URL_CHECK_IN_OUT = root + "check_in_out";
    public static String URL_ADD_SCHEDULE = root + "add_schedule";
    public static String URL_SEARCH_SCHEDULE = root + "search_schedule";
    public static String URL_SEARCH_CHECK = root + "search_check";
    public static String URL_MODIFY_SCHEDULE = root + "modify_schedule";

    public static String URL_ADD_OT = root + "add_ot";
    public static String URL_SEARCH_OT = root + "search_ot";
    public static String URL_UPDATE_OT = root + "update_ot";


    public static void logoutUser(SQLiteHandler db, SessionManager session, Activity activity) {
        session.setLogin(false);
        db.deleteUsers();

        // Launching the login activity
        Intent intent = new Intent(activity, LoginActivity.class);
        activity.startActivity(intent);
        activity.finish();
    }

    @SuppressLint("DefaultLocale")
    public static String timeFormate(int hourOfDay, int minute) {

        int hour = hourOfDay % 12;
        return String.format("%02d:%02d %s", hour == 0 ? 12 : hour,
                minute, hourOfDay < 12 ? "am" : "pm");
    }
}
