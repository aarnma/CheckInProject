package aaron.checkin.com.checkin.Helper;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhew on 2/28/18.
 **/

public class CheckHttp {
    Context context;
    View view;

//    public CheckHttp(Context context) {
//        this.context = context;
//    }

    public CheckHttp(Context context, View view) {
        this.context = context;
        this.view = view;
    }

    public void HttpJsonRequest(String url, JSONObject jsonObject,  final ActionCallback<JSONObject> cb) {
        MySingleton.getInstance(context).addToRequestQueue(new JsonObjectRequest(url, jsonObject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        cb.ok(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    if (error.networkResponse.statusCode == 200) {
                        cb.ok(null);
                    } else {
                        JSONObject json = new JSONObject(new String(
                                error.networkResponse.data));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    Snackbar.make(view, "Please check your Internet/Backend Server is shut", Snackbar.LENGTH_LONG).show();
                }
            }
        }));
    }
}
