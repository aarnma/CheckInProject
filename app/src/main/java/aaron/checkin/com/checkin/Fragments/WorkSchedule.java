package aaron.checkin.com.checkin.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.github.clans.fab.FloatingActionButton;
import com.github.tibolte.agendacalendarview.AgendaCalendarView;
import com.github.tibolte.agendacalendarview.CalendarPickerController;
import com.github.tibolte.agendacalendarview.models.BaseCalendarEvent;
import com.github.tibolte.agendacalendarview.models.CalendarEvent;
import com.github.tibolte.agendacalendarview.models.DayItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import aaron.checkin.com.checkin.Activities.AddSchedule;
import aaron.checkin.com.checkin.Helper.ActionCallback;
import aaron.checkin.com.checkin.Helper.CheckHttp;
import aaron.checkin.com.checkin.Helper.SQLiteHandler;
import aaron.checkin.com.checkin.Helper.SessionManager;
import aaron.checkin.com.checkin.Helper.URL;
import aaron.checkin.com.checkin.R;

import static aaron.checkin.com.checkin.Helper.URL.timeFormate;
import static android.app.Activity.RESULT_OK;


/**
 * Created by aaron on 6/11/2018.
 */

public class WorkSchedule extends Fragment implements CalendarPickerController {

    private AgendaCalendarView mAgendaCalendarView;
    private View view;

    private String name;
    private String right;

    String dateFormat = "yyyy-MM-dd";
    SimpleDateFormat sdf = new SimpleDateFormat(dateFormat, Locale.CANADA);

    String timeFormat = "HH:mm";
    SimpleDateFormat stf = new SimpleDateFormat(timeFormat, Locale.CANADA);

    Calendar minDate = Calendar.getInstance();
    Calendar maxDate = Calendar.getInstance();

    private String start_date;
    private String end_date;

    CalendarPickerController calendarPickerController;

    private Spinner staff;
    private SQLiteHandler db;
    private SessionManager session;
    private List<String> staffList = new ArrayList<>();


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_work_schedule, container, false);
        mAgendaCalendarView = view.findViewById(R.id.agenda_calendar_view);
        calendarPickerController = this;

        staff = getActivity().findViewById(R.id.staff_list);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.staff_name, R.layout.spinner_item);
        adapter.setDropDownViewResource(R.layout.spinner_draw);
        staff.setAdapter(adapter);

        db = new SQLiteHandler(getActivity());
        session = new SessionManager(getActivity());

        HashMap<String, String> user = db.getUserDetails();
        name = user.get("name");
        right = user.get("right");

        staffList=  Arrays.asList(getResources().getStringArray(R.array.staff_name));
        staff.setSelection(staffList.indexOf(name));
        staff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                name = adapterView.getItemAtPosition(i).toString();
                getEvents(name, start_date, end_date, calendarPickerController);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        FloatingActionButton add = view.findViewById(R.id.add_schedule);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getActivity(), AddSchedule.class),1);
            }
        });

        if (Integer.valueOf(right) < 7) {
            staff.setEnabled(false);
            add.setVisibility(View.GONE);
        }


        minDate.add(Calendar.YEAR, -1);
        minDate.set(Calendar.DAY_OF_MONTH, 1);
        maxDate.add(Calendar.YEAR, 1);


        Calendar c = Calendar.getInstance();
        maxDateOfMonth(c);
        getEvents(name, start_date, end_date, this);
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if(resultCode == RESULT_OK) {
                String name = data.getStringExtra("name");
                staff.setSelection(staffList.indexOf(name));
                getEvents(name, start_date, end_date, this);
            }
        }
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {

            staff = getActivity().findViewById(R.id.staff_list);
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                    R.array.staff_name, R.layout.spinner_item);
            adapter.setDropDownViewResource(R.layout.spinner_draw);
            staff.setAdapter(adapter);


            staffList=  Arrays.asList(getResources().getStringArray(R.array.staff_name));
            staff.setSelection(staffList.indexOf(name));
            staff.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    name = adapterView.getItemAtPosition(i).toString();
                    getEvents(name, start_date, end_date, calendarPickerController);
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });

            if (Integer.valueOf(right) < 7) {
                staff.setEnabled(false);
//                add.setVisibility(View.GONE);
            }


            getEvents(name, start_date, end_date, this);
        }
    }


    @Override
    public void onDaySelected(DayItem dayItem) {
    }

    @Override
    public void onEventSelected(CalendarEvent event) {
        if (Integer.valueOf(right) > 5 && event.getTitle().contains("From")) {

            Intent i = new Intent(getActivity(), AddSchedule.class);
            i.putExtra("start_time", stf.format(event.getStartTime().getTime()));
            i.putExtra("end_time", stf.format(event.getEndTime().getTime()));
            i.putExtra("date", sdf.format(event.getEndTime().getTime()));
            i.putExtra("name", staff.getSelectedItem().toString());

            startActivityForResult(i, 1);
        }

    }

    @Override
    public void onScrollToDate(Calendar calendar) {
        System.out.println("First Day of month: " + start_date);
        System.out.println("Last Day of month: " + end_date);

    }

    public void getEvents(final String name, final String from_date, final String to_date, final CalendarPickerController controller) {

        final JSONObject checkoutObject = new JSONObject();
        try {
            checkoutObject.put("name", name);
            checkoutObject.put("from_date", from_date);
            checkoutObject.put("to_date", to_date);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new CheckHttp(getActivity(), view).HttpJsonRequest(URL.URL_SEARCH_SCHEDULE, checkoutObject, new ActionCallback<JSONObject>() {
            @Override
            public void ok(JSONObject jsonObject) {

                List<CalendarEvent> events = new ArrayList<>();

                try {

                    if (jsonObject.getString("status").equals("500")) {
                        Snackbar.make(view, jsonObject.getString("msg"), Snackbar.LENGTH_LONG).show();
                    } else if (jsonObject.getString("status").equals("200")) {
                        JSONArray Jarray = jsonObject.getJSONArray("output");

                        for (int i = 0; i < Jarray.length(); i++) {
                            JSONObject object = Jarray.getJSONObject(i);
                            String date = object.getString("date");
                            String start = object.getString("start_time");
                            String end = object.getString("end_time");

                            String[] dateList = date.split("-");

                            Calendar startTime = Calendar.getInstance();
                            Calendar endTime = Calendar.getInstance();

                            String[] startList = start.split(":");
                            String[] endList = end.split(":");

                            startTime.set(Integer.parseInt(dateList[0]), Integer.parseInt(dateList[1]) - 1, Integer.parseInt(dateList[2]),
                                    Integer.parseInt(startList[0]), Integer.parseInt(startList[1]));
                            endTime.set(Integer.parseInt(dateList[0]), Integer.parseInt(dateList[1]) - 1, Integer.parseInt(dateList[2]),
                                    Integer.parseInt(endList[0]), Integer.parseInt(endList[1]));

                            BaseCalendarEvent event1 = new BaseCalendarEvent(name + "\n" + object.getString("date") + "\n"
                                    + "From: " + timeFormate(Integer.parseInt(startList[0]), Integer.parseInt(startList[1])) + "\n"
                                    + "To: " + timeFormate(Integer.parseInt(endList[0]), Integer.parseInt(endList[1])), "", "HCDF",
                                    ContextCompat.getColor(getActivity(), R.color.colorAccent), startTime, endTime, true);
                            events.add(event1);

                        }
                        getChecks(name,checkoutObject, controller, events);
                    } else {
                        mAgendaCalendarView.init(events, minDate, maxDate, Locale.getDefault(), controller);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }


    public void getChecks(final String name, JSONObject jsonbject, final CalendarPickerController controller, final List<CalendarEvent> events) {

        new CheckHttp(getActivity(), view).HttpJsonRequest(URL.URL_SEARCH_CHECK, jsonbject, new ActionCallback<JSONObject>() {
            @Override
            public void ok(JSONObject jsonObject) {

                try {

                    if (jsonObject.getString("status").equals("500")) {
                        Snackbar.make(view, jsonObject.getString("msg"), Snackbar.LENGTH_LONG).show();
                    } else if (jsonObject.getString("status").equals("200")) {

                        JSONArray Jarray = jsonObject.getJSONArray("output");

                        for (int i = 0; i < Jarray.length(); i++) {
                            JSONObject object = Jarray.getJSONObject(i);
                            String date = object.getString("date");
                            String start = object.getString("check_in_time");
                            String end = object.getString("check_out_time");

                            String[] dateList = date.split("-");
                            String[] startList = start.split(":");
                            String[] endList = end.split(":");

                            Calendar startTime = Calendar.getInstance();
                            Calendar endTime = Calendar.getInstance();

                            String out = "";
                            int color = ContextCompat.getColor(getActivity(), R.color.gray), hour = endTime.get(Calendar.HOUR),
                                    minute = endTime.get(Calendar.MINUTE);
                            if (!end.equals("")) {
                                out = timeFormate(Integer.parseInt(endList[0]), Integer.parseInt(endList[1]));
                                color = ContextCompat.getColor(getActivity(), R.color.resend_color);
                                hour = Integer.parseInt(endList[0]);
                                minute = Integer.parseInt(endList[1]);
                            }
                            startTime.set(Integer.parseInt(dateList[0]), Integer.parseInt(dateList[1]) - 1, Integer.parseInt(dateList[2]),
                                    Integer.parseInt(startList[0]), Integer.parseInt(startList[1]));
                            endTime.set(Integer.parseInt(dateList[0]), Integer.parseInt(dateList[1]) - 1, Integer.parseInt(dateList[2]), hour
                                    , minute);


                            BaseCalendarEvent event = new BaseCalendarEvent(name + "\n" + object.getString("date") + "\n"
                                    + "Check In: " + timeFormate(Integer.parseInt(startList[0]), Integer.parseInt(startList[1])) + "\n"
                                    + "Check Out: " + out, "", "HCDF",
                                    color, startTime, endTime, true);
                            events.add(event);

                        }
                    }
                    mAgendaCalendarView.init(events, minDate, maxDate, Locale.getDefault(), controller);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private void maxDateOfMonth(Calendar calendar) {

        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(year - 1, month, day);
        start_date = sdf.format(calendar.getTime());

        calendar.add(Calendar.YEAR, 2);
        end_date = sdf.format(calendar.getTime());

    }



}
