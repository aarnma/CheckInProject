package aaron.checkin.com.checkin.model;

public interface CursorModel {
    boolean hasMore();
}
