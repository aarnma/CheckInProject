package aaron.checkin.com.checkin.Fragments;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.cocosw.bottomsheet.BottomSheet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import aaron.checkin.com.checkin.Helper.ActionCallback;
import aaron.checkin.com.checkin.Helper.CheckHttp;
import aaron.checkin.com.checkin.Helper.ResistanceDragDistanceConvert;
import aaron.checkin.com.checkin.Helper.URL;
import aaron.checkin.com.checkin.R;
import aaron.checkin.com.checkin.adapter.RecyclerListAdapter;
import aaron.checkin.com.checkin.model.OpenProjectModel;

/**
 * Created by aaron on 6/11/2018.
 */

public class PendOT extends RecyclerFragment<OpenProjectModel> {

    private final List<OpenProjectModel> mItemList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_simple_item, menu);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getOriginAdapter().setItemList(mItemList);
        getHeaderAdapter().notifyDataSetChanged();

        getRecyclerRefreshLayout().setDragDistanceConverter(
                new ResistanceDragDistanceConvert(getScreenHeight(getActivity())));
    }

    private static int getScreenHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.heightPixels;
    }

    @Override
    protected RecyclerView.LayoutManager onCreateLayoutManager() {
        return new LinearLayoutManager(getActivity());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @NonNull
    @Override
    public RecyclerListAdapter createAdapter() {
        return new RecyclerListAdapter() {
            {
                addViewType(OpenProjectModel.class, new ViewHolderFactory<ViewHolder>() {
                    @Override
                    public ViewHolder onCreateViewHolder(ViewGroup parent) {
                        return new ItemViewHolder(parent);
                    }
                });
            }
        };
    }

    @Override
    protected InteractionListener createInteraction() {
        return new ItemInteractionListener();
    }


    private void getOTFromBackend(int page, final RequestListener listener) {

        final JSONObject otObject = new JSONObject();
        try {
            otObject.put("name", super.name);
            otObject.put("page", page);

        } catch (JSONException e) {
            e.printStackTrace();
        }


        new CheckHttp(getActivity(), getView()).HttpJsonRequest(URL.URL_SEARCH_OT, otObject, new ActionCallback<JSONObject>() {
            @Override
            public void ok(JSONObject jsonObject) {

                try {
                    if (jsonObject.getString("status").equals("200")) {
                        JSONArray Jarray = jsonObject.getJSONArray("output");

                        if(Jarray.length() <= 0){
                            if(mItemList.size()<=0){
                                listener.onSuccess(Collections.EMPTY_LIST);

                            }else{
                                listener.onFailed();
                                Snackbar.make(getView(), "No more data", Snackbar.LENGTH_SHORT).show();
                            }


                        }else{
                            List<OpenProjectModel> otList = new ArrayList<>();

                            for (int i = 0; i < Jarray.length(); i++) {
                                JSONObject object = Jarray.getJSONObject(i);
                                String date = object.getString("date");
                                String name = object.getString("name");
                                String ot = object.getString("ot");

                                if(object.getString("status").equals("p")){
                                    otList.add(new OpenProjectModel(date, name, "Over Time: " + ot , "#888888", "Pending"));
                                }else{
                                    otList.add(new OpenProjectModel(date, name, "Over Time: " + ot , "#DC143C", "Approved"));
                                }


                            }

                            listener.onSuccess(otList);
                        }


                    }else if (jsonObject.getString("status").equals("404")) {
                        listener.onSuccess(Collections.EMPTY_LIST);

                    }
                    else {
                        listener.onFailed();
                        Snackbar.make(getView(), jsonObject.getString("msg"), Snackbar.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    private interface RequestListener {
        void onSuccess(List<OpenProjectModel> openProjectModels);

        void onFailed();
    }

    private class ItemInteractionListener extends InteractionListener {
        @Override
        public void requestRefresh() {
            getOTFromBackend(1,new RequestListener() {
                @Override
                public void onSuccess(List<OpenProjectModel> openProjectModels) {
                    page = 2;
                    mItemList.clear();
                    mItemList.addAll(openProjectModels);
                    getHeaderAdapter().notifyDataSetChanged();
                    ItemInteractionListener.super.requestRefresh();
                }

                @Override
                public void onFailed() {
                    ItemInteractionListener.super.requestFailure();
                }
            });
        }

        @Override
        public void requestMore() {
            getOTFromBackend(page,new RequestListener() {
                @Override
                public void onSuccess(List<OpenProjectModel> openProjectModels) {
                    mItemList.addAll(openProjectModels);
                    getHeaderAdapter().notifyDataSetChanged();
                    ItemInteractionListener.super.requestMore();
                    page++;
                }

                @Override
                public void onFailed() {
                    ItemInteractionListener.super.requestMore();
                }
            });
        }
    }

    private class ItemViewHolder extends RecyclerListAdapter.ViewHolder<OpenProjectModel> {
        private final TextView mTvTitle;
        private final TextView mTvContent;
        private final TextView mTvAuthor;
        private final TextView mTvStatus;

        private final LinearLayout mLlContentPanel;

        public ItemViewHolder(@NonNull ViewGroup parent) {
            super(LayoutInflater.from(getActivity()).inflate(R.layout.simple_list_item, parent, false));

            mTvTitle = (TextView) itemView.findViewById(R.id.title);
            mTvContent = (TextView) itemView.findViewById(R.id.content);
            mTvAuthor = (TextView) itemView.findViewById(R.id.author);
            mTvStatus = itemView.findViewById(R.id.status);

            mLlContentPanel = (LinearLayout) itemView.findViewById(R.id.content_panel);
        }

        @Override
        public void bind(final OpenProjectModel item, final int position) {
            mTvTitle.setText(item.getTitle());
            mTvContent.setText(item.getContent());
            mTvAuthor.setText(item.getAuthor());

            final String status = item.getmStatus();
            mTvStatus.setText(status);

            mLlContentPanel.setBackgroundColor(Color.parseColor(item.getColor()));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(Integer.parseInt(right) > 5){
                        BottomSheet.Builder btsheet = new BottomSheet.Builder(getActivity());

                        final String date = mTvTitle.getText().toString();

                        if(status.equals("Pending")){
                            btsheet.sheet(R.menu.pend_menu);
                            btsheet.title("Are you sure to approve the OT on " + date + " ?");
                        }else{
                            btsheet.sheet(R.menu.approve_menu);
                            btsheet.title("Are you sure to disapprove the OT on " + date + " ?");
                        }

                        btsheet.listener(new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        switch (which) {
                                            case R.id.approve:

                                                JSONObject jsonObject = new JSONObject();
                                                String old_status = status.equals("Pending")?"p":"a";
                                                String new_status = status.equals("Pending")?"a":"p";

                                                try {
                                                    jsonObject.put("name", name);
                                                    jsonObject.put("date", date);
                                                    jsonObject.put("old_status", old_status);
                                                    jsonObject.put("new_status", new_status);
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                changeOTstatus(jsonObject);
                                                break;

                                        }
                                    }
                                }).show();
                    }
                }
            });
        }
    }


    private void changeOTstatus(JSONObject jsonObject){
        new CheckHttp(getActivity(), getView()).HttpJsonRequest(URL.URL_UPDATE_OT, jsonObject, new ActionCallback<JSONObject>() {
            @Override
            public void ok(JSONObject jsonObject) {

                try {
                    if(jsonObject.getString("status").equals("200")){
                        refresh();
                    }else{
                        Snackbar.make(getView(), jsonObject.getString("msg"), Snackbar.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }

}