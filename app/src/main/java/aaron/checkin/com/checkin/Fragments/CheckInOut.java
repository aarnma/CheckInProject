package aaron.checkin.com.checkin.Fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AnalogClock;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextClock;

import org.apache.http.conn.util.InetAddressUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.ByteOrder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import aaron.checkin.com.checkin.Helper.ActionCallback;
import aaron.checkin.com.checkin.Helper.CheckHttp;
import aaron.checkin.com.checkin.Helper.SQLiteHandler;
import aaron.checkin.com.checkin.Helper.SessionManager;
import aaron.checkin.com.checkin.Helper.SharedPreference;
import aaron.checkin.com.checkin.Helper.URL;
import aaron.checkin.com.checkin.R;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.content.Context.WIFI_SERVICE;
import static cn.pedant.SweetAlert.SweetAlertDialog.NORMAL_TYPE;
import static cn.pedant.SweetAlert.SweetAlertDialog.WARNING_TYPE;
import static java.lang.Math.round;

//import java.text.DateFormat;
//import java.text.SimpleDateFormat;
//import java.util.Calendar;

/**
 * Created by aaron on 6/11/2018.
 */

public class CheckInOut extends Fragment implements View.OnClickListener {
    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    DateFormat timeFormat = new SimpleDateFormat("HH:mm");

    private SQLiteHandler db;
    private SessionManager session;
    private String name;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_check, container, false);

        AnalogClock ac = view.findViewById(R.id.analogClock);
        TextClock dc = view.findViewById(R.id.digitalClock);

        Button checkin = view.findViewById(R.id.btnCheckIn);
        Button checkout = view.findViewById(R.id.btnCheckOut);

        db = new SQLiteHandler(getActivity());
        session = new SessionManager(getActivity());

        HashMap<String, String> user = db.getUserDetails();
        name = user.get("name");

        checkin.setOnClickListener(this);
        checkout.setOnClickListener(this);
        return view;
    }


    @Override
    public void onClick(final View view) {
        String check_in = SharedPreference.getInstance(getActivity())
                .getStringValue(name + "_check_in_date", "");

        String check_out = SharedPreference.getInstance(getActivity())
                .getStringValue(name + "_check_out_date", "");

        final Calendar cal = Calendar.getInstance();
        final String current_time = dateFormat.format(cal.getTime());


        switch (view.getId()) {
            case R.id.btnCheckIn:

                if (wifiIpAddress(getActivity()).equals("192.168.0.17")) {
                    if (check_in.equals(current_time)) {
                        Snackbar.make(view, "Hi " + name + ", You have checked in yet today", Snackbar.LENGTH_LONG).show();
                    } else {

                        new SweetAlertDialog(getActivity(), NORMAL_TYPE)
                                .setTitleText("Hi " + name + ", Are you sure to check in?")
                                .setConfirmText("Yes")
                                .setCancelText("No")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {

                                        final JSONObject checkinObject = new JSONObject();
                                        try {
                                            checkinObject.put("name", name);
                                            checkinObject.put("type", "check in");

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        new CheckHttp(getActivity(), view).HttpJsonRequest(URL.URL_CHECK_IN_OUT, checkinObject, new ActionCallback<JSONObject>() {
                                            @Override
                                            public void ok(JSONObject jsonObject) {

                                                try {
                                                    if (jsonObject.getString("status").equals("200")) {
                                                        SharedPreference.getInstance(getActivity()).setValue(name + "_check_in_date", current_time);
                                                        checkLate(jsonObject.getString("expect_time")
                                                                , jsonObject.getString("actual_time"), jsonObject.getString("date"), "in");
                                                    } else {
                                                        Snackbar.make(view, jsonObject.getString("msg"), Snackbar.LENGTH_LONG).show();
                                                    }

                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                            }
                                        });
                                        sweetAlertDialog.cancel();
                                    }
                                })
                                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.cancel();
                                    }
                                })
                                .show();
                    }
                } else {
                    Snackbar.make(view, "You are not in HCDF, please connect HCDF's wifi for check in", Snackbar.LENGTH_LONG).show();
                }

                break;

            case R.id.btnCheckOut:
                if (check_in.equals(current_time) && !check_out.equals(current_time)) {

                    new SweetAlertDialog(getActivity(), NORMAL_TYPE)
                            .setTitleText("Hi " + name + ", Are you sure to check out?")
                            .setConfirmText("Yes")
                            .setCancelText("No")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {

                                    final JSONObject checkoutObject = new JSONObject();
                                    try {
                                        checkoutObject.put("name", name);
                                        checkoutObject.put("type", "check out");

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    new CheckHttp(getActivity(), view).HttpJsonRequest(URL.URL_CHECK_IN_OUT, checkoutObject, new ActionCallback<JSONObject>() {
                                        @Override
                                        public void ok(JSONObject jsonObject) {

                                            try {

                                                if (jsonObject.getString("status").equals("200")) {
                                                    SharedPreference.getInstance(getActivity()).setValue(name + "_check_out_date", current_time);

                                                    checkLate(jsonObject.getString("expect_time")
                                                            , jsonObject.getString("actual_time"), jsonObject.getString("date"), "out");

                                                } else {
                                                    Snackbar.make(view, jsonObject.getString("msg"), Snackbar.LENGTH_LONG).show();
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                        }
                                    });
                                    sweetAlertDialog.cancel();
                                }
                            })
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.cancel();
                                }
                            })
                            .show();


                } else if (check_out.equals(current_time)) {
                    Snackbar.make(view, "Hi " + name + ", You have checked out yet today", Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(view, "Hi " + name + ", You have not checked in yet, please check in first", Snackbar.LENGTH_LONG).show();
                }

                break;

        }
    }


    private String wifiIpAddress(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(WIFI_SERVICE);
        int ipAddress = wifiManager.getConnectionInfo().getIpAddress();

        // Convert little-endian to big-endianif needed
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

        String ipAddressString;
        try {
            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
        } catch (UnknownHostException ex) {
            Log.e("WIFIIP", "Unable to get host address.");
            ipAddressString = null;
        }

        return ipAddressString;
    }

    private void checkLate(String expect, String actual, final String date, String type) {

        try {
            Date actual_date = timeFormat.parse(actual);
            Date expect_date = timeFormat.parse(expect);

            if (!actual_date.after(expect_date)) {
                switch (type) {
                    case "in":
                        new SweetAlertDialog(getActivity(), NORMAL_TYPE)
                                .setTitleText("Hi " + name + ", you check in at " + actual + " on " + date)
                                .setContentText("Have a good day!")
                                .setConfirmText("OK")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.cancel();
                                    }
                                })
                                .show();
                        break;
                    case "out":
                        new SweetAlertDialog(getActivity(), NORMAL_TYPE)
                                .setTitleText("Hi " + name + ", you check out at " + actual + " on " + date)
                                .setContentText("Have a good night!")
                                .setConfirmText("OK")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.cancel();
                                    }
                                })
                                .show();
                        break;
                }

            } else {
                long diff = actual_date.getTime() - expect_date.getTime();
                final long diffMinutes = diff / (60 * 1000) % 60;
                final long diffHours = diff / (60 * 60 * 1000) % 24;


                switch (type) {
                    case "in":
                        new SweetAlertDialog(getActivity(), WARNING_TYPE)
                                .setTitleText("Hi " + name + ", you check in at " + actual + " on " + date)
                                .setContentText("You are late for " + diffHours + " hour(s) " + diffMinutes + " min(s)")
                                .setConfirmText("OK")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        sweetAlertDialog.cancel();
                                    }
                                })
                                .show();
                        break;
                    case "out":
                        if (diffMinutes > 0 || diffHours > 0) {

                            new SweetAlertDialog(getActivity(), WARNING_TYPE)
                                    .setTitleText("Hi " + name + ", you check out at " + actual + " on " + date)
                                    .setContentText("You are over time for " + diffHours + " hour(s) " + diffMinutes + " min(s)" + "\n" +
                                            "Would you like to add it to pended OT and approved by your manager?")
                                    .setConfirmText("OK")
                                    .setCancelText("No")
                                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {

                                            double ot = diffHours + (diffMinutes / 60.0);

                                            final JSONObject otObject = new JSONObject();
                                            try {
                                                otObject.put("name", name);
                                                otObject.put("ot", round(ot * 100.0) / 100.0);
                                                otObject.put("date", date);

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            new CheckHttp(getActivity(), getView()).HttpJsonRequest(URL.URL_ADD_OT, otObject, new ActionCallback<JSONObject>() {
                                                @Override
                                                public void ok(JSONObject jsonObject) {
                                                    try {
                                                        Snackbar.make(getView(), jsonObject.getString("msg"), Snackbar.LENGTH_LONG).show();
                                                    } catch (JSONException e) {
                                                        e.printStackTrace();
                                                    }

                                                }
                                            });
                                            sweetAlertDialog.cancel();
                                        }
                                    })
                                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                        @Override
                                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                                            sweetAlertDialog.cancel();
                                        }
                                    })
                                    .show();
                        }
                        break;
                }


            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }
}